#!/usr/bin/env python3

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from flask import Flask, request
import craw

app = Flask(__name__)

@app.route('/')
def hello_server():
    return "hello\n"

@app.route('/craw')
def crawling():
    keyword = request.args.get('keyword')
    test = craw.flask_test(keyword)
    return "hello " + test + "\n"


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=1234, debug=True)