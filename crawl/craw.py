#!/usr/bin/env python3

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import requests, json, time
from bs4 import BeautifulSoup

def shop_crawl(keyword):
    start_time = time.time()
    query = 'https://search.shopping.naver.com/search/all?query='
    query += keyword

    response = requests.get(query)
    soup = BeautifulSoup(response.text, 'html.parser')
    
    raw_data = str(soup.find('script', id='__NEXT_DATA__', type='application/json'))
    str_data = raw_data[raw_data.index('{"item":') : raw_data.index('</')]
    str_data = str_data[ : str_data.index('],"total":')]
    item_list = [ x[:-1] for x in str_data.split('{"item":')]
    item_list[-1] += '}'

    ##################################################
    
    print("--------------------------")
    
    for x in item_list:
        print(x)

    print("--------------------------")

    ##################################################
    end_time = time.time()
    print("time : {time} ".format(time = (end_time - start_time)))

def flask_test(keyword):
    return "111" + keyword + "222\n"

if __name__ == "__main__":
    print('please enter a keyword : ')
    keyword = str(input())
    # keyword = 'ML574EGW'
    shop_crawl(keyword)